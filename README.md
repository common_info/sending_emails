# General #

## What is it? ##

It's a small utility to get information from the Youtrack and send it to users through the emails.

## What is required to use? ##

You need only the Python 3.10+ interpretator, the `requests` (2.28.1+) and `loguru` (0.6.0+) libs.

## Is it safe? ##

Yes, no information is leaked or retransmitted to the third parties.
