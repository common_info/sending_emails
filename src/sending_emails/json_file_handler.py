import json
from pathlib import Path
from typing import Optional

from loguru import logger

__all__ = ["JSONFile"]


class _JSONReader:
    def __init__(self, file_name: str):
        self.file_name: str = file_name
        self.path: Path = Path(__file__).with_name(self.file_name)

    def __str__(self):
        return f"{self.__class__.__name__}: {self.file_name}"

    def __repr__(self):
        return f"<{self.__class__.__name__}(file_name={self.file_name})>"

    def read(self) -> Optional[dict[str, list[str]]]:
        try:
            with open(self.path, "r") as file:
                content: dict[str, list[str]] = json.load(file)
        except FileNotFoundError as e:
            logger.error(f"{e.__class__.__name__}, file: {e.filename}\n{e.strerror}")
            return
        except IsADirectoryError as e:
            logger.error(f"{e.__class__.__name__}, file: {e.filename}\n{e.strerror}")
            return
        except PermissionError as e:
            logger.error(f"{e.__class__.__name__}, file: {e.filename}\n{e.strerror}")
            return
        except OSError as e:
            logger.error(f"{e.__class__.__name__}, file: {e.filename}\n{e.strerror}")
            return
        else:
            return content


class _JSONWriter:
    def __init__(self, file_name: str):
        self.file_name: str = file_name
        self.path: Path = Path(__file__).with_name(self.file_name)

    def __str__(self):
        return f"{self.__class__.__name__}: {self.file_name}"

    def __repr__(self):
        return f"<{self.__class__.__name__}(file_name={self.file_name})>"

    def write(self, text: dict[str, list[str]]):
        try:
            with open(self.path, "w", encoding="utf8") as file:
                file.write(json.dumps(text, indent=2))
        except FileNotFoundError as e:
            logger.error(f"{e.__class__.__name__}, файл: {e.filename}\n{e.strerror}")
            return
        except IsADirectoryError as e:
            logger.error(f"{e.__class__.__name__}, файл: {e.filename}\n{e.strerror}")
            return
        except PermissionError as e:
            logger.error(f"{e.__class__.__name__}, файл: {e.filename}\n{e.strerror}")
            return
        except OSError as e:
            logger.error(f"{e.__class__.__name__}, файл: {e.filename}\n{e.strerror}")
            return


class JSONFile:
    content = None
    _instance = None

    def __new__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = super().__new__(cls)
        return cls._instance

    def __init__(self, file_name: str = "issues.json"):
        self.file_name: str = file_name
        self.path: Path = Path(__file__).with_name(self.file_name)

    def __str__(self):
        return f"{self.__class__.__name__}: {self.file_name}"

    def __repr__(self):
        return f"<{self.__class__.__name__}(file_name={self.file_name})>"

    def json_reader(self) -> _JSONReader:
        return _JSONReader(self.file_name)

    def json_writer(self) -> _JSONWriter:
        return _JSONWriter(self.file_name)

    def read(self):
        self.content = self.json_reader().read()

    def write(self, text: dict[str, list[str]] = None):
        if text is not None:
            self.json_writer().write(text)

    def __getitem__(self, item):
        return self.content.__getitem__(item)

    def __setitem__(self, key, value):
        self.content.__setitem__(key, value)

    def __iter__(self):
        return iter(self.content)

    def __len__(self):
        return sum(len(self[user]) for user in self.content.keys())

    def __bool__(self):
        return len(self) > 0

    def __contains__(self, item):
        return item in self.content.keys()

    def nullify(self):
        for login in self:
            self[login] = []
        self.write(self.content)
