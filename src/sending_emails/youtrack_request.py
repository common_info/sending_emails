import json
from datetime import date
from os import environ
from typing import Optional, Union

from loguru import logger
from requests import Session, PreparedRequest, Request, Response
from requests.exceptions import InvalidURL, InvalidHeader, HTTPError, RequestException, JSONDecodeError

from youtrack_response import ResponseYoutrack

__all__ = ["RequestYoutrack"]


class RequestYoutrack:
    """
    Specifies the instance of the API request to the YouTrack.

    Params:
        session --- the HTTP connection session, Session;\n
        params --- the additional headers of the request to accurate the request, tuple[tuple[str, str], ...];\n
        url --- the webhook to send the API request, str;\n
        headers --- the mandatory headers of the request, default: the HEADERS constant;\n
        method --- the HTTP method to use, default: GET;

    Functions:
        _convert_long_date(long: int or None) --- convert the timestamp to date, date;\n
        send_request() --- send the request to the YouTrack and get the response in the JSON format, list[dict];\n
        parse_response() --- handle the response to represent as the ResponseYoutrack objects,\
        list[ResponseYoutrack];
    """

    _request_num: int = 0
    _auth_token: str = environ["AUTH_TOKEN"]
    _headers_tuple: tuple[tuple[str, str], ...] = (
        ("Authorization", f"Bearer {_auth_token}"),
        ("Accept", "application/json"),
        ("Content-Type", "application/json"),
    )
    _url: str = "https://youtrack.protei.ru/api/issues"

    def __init__(
            self,
            session: Optional[Session],
            params: tuple[tuple[str, str], ...],
            url: str = _url,
            headers: tuple[tuple[str, str], ...] = _headers_tuple,
            method: str = "get"):
        self._session: Session = session
        self.params: tuple[tuple[str, str], ...] = params
        self.url: str = url
        self.headers: dict[str, str] = dict([(header[0], header[1]) for header in headers])
        self.method: str = method
        self.response: list[dict] = []

    def __repr__(self):
        return f"<{self.__class__.__name__}(params={self.params}, url={self.url}, headers={self.headers}, " \
               f"method={self.method})>"

    def __str__(self):
        return f"Запрос: {self.method.upper()} {self.url}\n{self.params}\n{self.headers}"

    def __format__(self, format_spec):
        if format_spec == "output":
            return f"RequestYoutrack: params = {self.params}, url = {self.url}, headers = {self.headers}, " \
                   f"method = {self.method}"

    @property
    def session(self) -> Session:
        """
        Gets the HTTP session.

        :return: the HTTP session.
        :rtype: Session
        """
        return self._session

    @session.setter
    def session(self, value):
        """
        Specifies the HTTP session.

        :param value: the HTTP session
        """
        if isinstance(value, Session):
            self._session = value
        else:
            logger.error(f"Значение должно быть типа Session, получено {type(value)}")

    @staticmethod
    def _convert_long_date(long: Optional[int]) -> date:
        """
        Convert the long int value to the datetime.\n
        The considered value is provided up to milliseconds, so it is required to divide by 1000.

        :param int long: the timestamp
        :return: the date associated with the timestamp.
        :rtype: date
        """
        return date.fromtimestamp(long / 1000) if long else date(1, 1, 1)

    def send_request(self):
        """
        Attempts to send the request and get the response from the YouTrack.\n
        Validates any possible connection, request, or handling problems.

        :return: the response to the request in the JSON format.
        :rtype: list[dict] or None
        """
        try:
            prepared_request: PreparedRequest = Request(
                method=self.method, url=self.url, headers=self.headers, params=self.params).prepare()
            logger.debug(f"Запрос #{self.__class__._request_num}")
            logger.debug(f"URL: {prepared_request.path_url}\nHeaders: {self.headers}\nParams: {self.params}")
            response: Response = self.session.send(prepared_request)
            logger.debug(f"Ответ: {json.dumps(response.json(), ensure_ascii=False)}")
            json_response: list[dict] = response.json()
        except InvalidURL as e:
            logger.error(
                f"{e.__class__.__name__}. Incorrect request URL.\n{e.strerror}\n"
                f"Request: {e.request}\nResponse: {e.response}")
            raise
        except InvalidHeader as e:
            logger.error(
                f"{e.__class__.__name__}. Incorrect headers.\n{e.strerror}\n"
                f"Request: {e.request}\nResponse: {e.response}")
            raise
        except ConnectionError as e:
            logger.error(f"{e.__class__.__name__}. Connection failed.\n{e.strerror}")
            raise
        except HTTPError as e:
            logger.error(
                f"{e.__class__.__name__}. General HTTP request error.\n{e.strerror}\n"
                f"Request: {e.request}\nResponse: {e.response}")
            raise
        except RequestException as e:
            logger.error(
                f"{e.__class__.__name__}. Main request error.\n{e.strerror}\n"
                f"Request: {e.request}\nResponse: {e.response}")
            raise
        except OSError as e:
            logger.error(f"{e.__class__.__name__}. Global error occurred.\n{e.strerror}")
            raise
        except JSONDecodeError as e:
            logger.error(
                f"{e.__class__.__name__}. JSON decoding error occurred.\n{e.strerror}\n"
                f"Request: {e.request}\nResponse: {e.response}")
            raise
        else:
            self.__class__._request_num += 1
            self.response = json_response

    def parse_response(self) -> Optional[list[ResponseYoutrack]]:
        """
        Converts the response to the RequestResult objects.

        :return: the RequestResult instances.
        :rtype: list[ResponseYoutrack]
        """
        self.send_request()
        parsed_items: list[ResponseYoutrack] = []
        if not self.response:
            logger.error("Ответ на запрос пуст или запрос некорректен")
            return list()
        for response_item in self.response:
            issue: str = response_item["idReadable"]
            summary: str = response_item["summary"]
            deadline: date = date(1, 1, 1)
            state: str = ""
            priority: str = ""
            item: dict[str, Union[str, int, dict, None]]
            for item in response_item["customFields"]:
                # specify the issue state
                if item["$type"] in ("StateIssueCustomField", "StateMachineIssueCustomField"):
                    state = item["value"]["name"]
                # specify the issue deadline
                elif item["$type"] == "DateIssueCustomField":
                    deadline = self._convert_long_date(item["value"])
                # specify the issue priority
                elif item["$type"] == "SingleEnumIssueCustomField":
                    priority = item["value"]["name"]
                else:
                    continue
            # nullify the improper date or stringify the deadline date
            if deadline == date(1, 1, 1):
                final_deadline = ""
            else:
                final_deadline = deadline.isoformat()
            parsed_items.append(ResponseYoutrack(issue, state, summary, final_deadline, priority))
        return parsed_items
