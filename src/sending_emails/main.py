from init_logger import complete_configure_custom_logging
from json_file_handler import JSONFile
from mail_server import MessageYoutrack
from user_processing import FullUserProcessor, User

json_file_name: str = "issues.json"


@complete_configure_custom_logging("sending_emails")
def main(*, is_test: bool = False, is_init: bool = False):
    json_file: JSONFile = JSONFile()
    content: dict[str, list[str]] = json_file.json_reader().read()

    logins = [*content.keys()]

    full_user_processor: FullUserProcessor = FullUserProcessor(json_file_name, is_test=is_test)
    all_messages_youtrack: list[MessageYoutrack] = []

    for login in logins:
        user: User = User(json_file_name, login, is_test=is_test, is_init=is_init)
        full_user_processor.users.append(user)
        all_messages_youtrack.append(user.message_youtrack())

    if all_messages_youtrack:
        full_user_processor.smtp_server.server()
        full_user_processor.send_mails(all_messages_youtrack)

    json_file.write(full_user_processor.to_json_file())

if __name__ == "__main__":
    main(is_test=False, is_init=False)
