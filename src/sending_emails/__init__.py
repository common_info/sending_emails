import logging
from typing import Any

from loguru import logger

from init_logger import HandlerType, LoggingLevel, LoggerConfiguration, InterceptHandler

handlers: dict[HandlerType, LoggingLevel] = {
    "stream": "INFO",
    "file_rotating": "DEBUG"
}

file_name: str = "sending_emails"

logger_configuration: LoggerConfiguration = LoggerConfiguration(file_name, handlers)

stream_handler: dict[str, Any] = logger_configuration.stream_handler()
rotating_file_handler: dict[str, Any] = logger_configuration.rotating_file_handler()

logger.configure(handlers=[stream_handler, rotating_file_handler])

logging.basicConfig(handlers=[InterceptHandler()], level=0)
