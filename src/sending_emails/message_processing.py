from textwrap import dedent
from typing import Sequence, Optional

from loguru import logger

from json_file_handler import JSONFile
from youtrack_response import ResponseYoutrack

__all__ = ["MessageProcessor"]


class _IssueFilter:
    def __init__(self, json_file: JSONFile):
        self.json_file: JSONFile = json_file
        self.json_file_content: dict[str, list[str]] = dict()

    @property
    def content(self):
        return self.json_file.json_reader().read()

    def __getitem__(self, item):
        try:
            value = self.content.get(item)
        except KeyError as e:
            logger.error(f"{e.__class__.__name__}, {str(e)}")
            return
        except TypeError as e:
            logger.error(f"{e.__class__.__name__}, {str(e)}")
            return
        except OSError as e:
            logger.error(f"{e.__class__.__name__}, file name: {e.filename}, {e.strerror}")
            return
        else:
            return value

    def __setitem__(self, key, value):
        try:
            self.content[key] = []
            self.content[key] = list(*value)
        except KeyError as e:
            logger.error(f"{e.__class__.__name__}, {str(e)}")
            return
        except TypeError as e:
            logger.error(f"{e.__class__.__name__}, {str(e)}")
            return
        except ValueError as e:
            logger.error(f"{e.__class__.__name__}, {str(e)}")
            return
        except OSError as e:
            logger.error(f"{e.__class__.__name__}, file name: {e.filename}, {e.strerror}")
            return

    def get_issue_filter_user(self, login: str, responses: list[ResponseYoutrack] = None):
        if responses is None:
            responses = []
        if login not in self.content.keys():
            logger.error(f"Логин {login} не найден в списке пользователей")
            return
        return _IssueFilterUser(self.json_file, login, responses)


class _IssueFilterUser(_IssueFilter):
    def __init__(self,
                 json_file: JSONFile,
                 login: str,
                 responses: Sequence[ResponseYoutrack] = None):
        super().__init__(json_file)
        self.login: str = login
        self._responses: list[ResponseYoutrack] = [*responses] if responses else []

    def __repr__(self):
        return f"<{self.__class__.__name__}(json_file={str(self.json_file_content[self.login])}," \
               f"login={self.login}, responses={self._responses})>"

    def __str__(self):
        return f"Фильтр:\nJSON-файл -- {self.json_file_content[self.login]}, пользователь {self.login}\n" \
               f"Запросы Youtrack: {self._responses}"

    @classmethod
    def from_issue_filter(
            cls,
            issue_filter: _IssueFilter,
            login: str,
            responses: list[ResponseYoutrack] = None):
        if responses is None:
            responses = []
        return cls(issue_filter.json_file, login, responses)

    @property
    def _issues_json(self):
        return self.content[self.login]

    @property
    def _issues_youtrack(self) -> Optional[set[str]]:
        return {response.issue for response in self._responses} if self._responses else set()

    def new_responses(self) -> Optional[list[ResponseYoutrack]]:
        if not self.new_issues:
            logger.info(f"Новых запросов нет")
            return
        logger.info(f"Новые запросы: {self.new_issues}")
        return list(filter(lambda x: x.issue in self.new_issues, self._responses))

    @property
    def common_issues(self) -> set[str]:
        return set(self._issues_json).intersection(set(self._issues_youtrack))

    @property
    def new_issues(self) -> set[str]:
        return set(self._issues_youtrack).difference(set(self._issues_json))

    def joined_issues(self) -> list[str]:
        _common = [*self.common_issues] if self.common_issues else []
        _new = [*self.new_issues] if self.new_issues else []
        return [*self.common_issues, *self.new_issues]


class _MessageTextUser:
    _message_init: str = dedent("""\
        Добрый день, $USERNAME$.
        
        Попытка номер 2 отправки оповещений о назначенных задачах.
        Вкратце, основная идея --- рассылка оповещений при добавлении пользователей в задачу Youtrack в поле 
        Исполнители_*.
        Если были назначены задачи, то придет сообщение подобное этому, в котором будут перечислены ТОЛЬКО НОВЫЕ задачи.
        Если же задачи со вчерашнего дня не добавились, то ничего на почтовый ящик не придет, чтобы не захламлять его 
        лишними сообщениями.
        Обновление статуса, завершение задачи, изменение дедлайна, добавление комментариев и т.п. не активируют 
        рассылку.
        Предложения, замечания и пожелания приветствуются. А теперь по делу:\n
        Текущие незавершенные задачи на YouTrack, назначенные через поле Исполнители_*:\n""")
    _message_start: str = dedent("""\
        Добрый день, $USERNAME$.
        
        Краткая информация о текущих задачах на YouTrack, назначенных через поле Исполнители_*:
        """)
    _message_end: str = dedent("""\
        Обратная связь: tarasov-a@protei.ru / @andrewtar (https://t.me/andrewtar)""")
    _message_end_english: str = dedent("""\
    If the message above is a mess of rubbish characters and not readable, please notify about the problem, 
    thank you.""")
    _sentinel: str = "--------------------"

    def __init__(
            self,
            login: str,
            responses: Optional[list[ResponseYoutrack]] = None):
        if responses is None:
            responses = []
        self.login: str = login
        self.responses: list[ResponseYoutrack] = responses

    def __repr__(self):
        return f"<{self.__class__.__name__}(login={self.login}, responses={self.responses})>"

    def __str__(self):
        return f"Запросы с Youtrack:\n{[str(response) for response in self.responses]}"

    @property
    def _sorted_messages(self) -> Optional[str]:
        if self.responses:
            return "\n".join(str(response) for response in sorted(self.responses))
        return "Задач нет.\n"

    def message_text(self, is_init: bool = False) -> Optional[str]:
        if is_init:
            _message_structure: tuple[str, ...] = (
                self._message_init,
                self._sorted_messages,
                self._sentinel,
                self._message_end,
                self._message_end_english)
            return "\n".join(_message_structure)

        if not is_init:
            if not self.responses:
                return
            _message_structure: tuple[str, ...] = (
                self._message_start,
                self._sorted_messages,
                self._sentinel,
                self._message_end,
                self._message_end_english)
            return "\n".join(_message_structure)


class MessageProcessor:
    def __init__(
            self,
            json_file: JSONFile,
            login: str,
            responses: list[ResponseYoutrack] = None):
        if responses is None:
            responses = []
        self._json_file: JSONFile = json_file
        self._login: str = login
        self._responses: list[ResponseYoutrack] = responses

    def _issue_filter(self) -> _IssueFilter:
        return _IssueFilter(self._json_file)

    def _issue_filter_user(self) -> _IssueFilterUser:
        return self._issue_filter().get_issue_filter_user(self._login, self._responses)

    def _new_responses(self) -> list[ResponseYoutrack]:
        return self._issue_filter_user().new_responses()

    def _message_converter_user(self) -> _MessageTextUser:
        return _MessageTextUser(self._login, self._new_responses())

    def message_text(self, is_init: bool = False) -> Optional[str]:
        return self._message_converter_user().message_text(is_init)

    def to_json(self) -> tuple[str, list[str]]:
        return self._login, self._issue_filter_user().joined_issues()
