from functools import cached_property
from typing import Optional, Sequence

from loguru import logger
from requests import Session

from json_file_handler import JSONFile
from mail_server import MailServer, MessageAttributes, MessageYoutrack
from message_processing import MessageProcessor
from youtrack_request import RequestYoutrack
from youtrack_response import ResponseYoutrack

__all__ = ["User", "FullUserProcessor"]


class _RequestParams:
    """
    Specifies the params attribute of the request.

    Params:
        login --- the user login, str;

    Properties:
        _params_field --- the "fields" params value, str;\n
        _params_state --- the "State" params value, str;\n
        _params_assignees --- the Assignee_{} query value, str;\n
        _params_kwargs --- the query value, str;\n
        _params_query --- the "query" params value, str;\n
        params --- the params value of the request, tuple[tuple[str, str], ...];\n
    """
    _states_not_completed: str = ",".join(("Active", "Discuss", "New", "Paused", "Review"))
    _assignees_project: tuple[str] = (
        "Исполнители_Doc", "Исполнители_Doc_ST", "Исполнители_Archive", "Исполнители_Archive_ST")
    _params_custom_field_state: str = "State"
    _params_custom_field_deadline: str = "Дедлайн"
    _params_custom_field_priority: str = "Priority"

    def __init__(self, login: str, **kwargs):
        self.login: str = login
        for k, v in kwargs.items():
            setattr(self, k, v)
        self._attrs: list[str] = [k for k in kwargs]

    def __repr__(self):
        kwargs = {key: getattr(self, key) for key in self._attrs}
        return f"<{self.__class__.__name__}(login={self.login}, kwargs={kwargs})>"

    def __str__(self):
        params: dict[str, str] = {f"{attr}": f"{value}" for attr, value in self.params}
        return f"{self.__class__.__name__}:\nParams: {params}"

    @cached_property
    def _params_field(self) -> str:
        """
        Gets the "field" params value.

        :return: the params attribute value.
        :rtype: str
        """
        return ",".join(
            ("idReadable", "summary",
             "customFields(value,value(name),value(name,login),projectCustomField(field(name)))")
        )

    @cached_property
    def _params_state(self) -> str:
        """
        Gets the "State" params value.

        :return: the params attribute value.
        :rtype: str
        """
        return f"State: {self._states_not_completed}"

    @cached_property
    def _params_assignees(self) -> str:
        """
        Gets the "Assignees_{}" params value.

        :return: the params attribute value.
        :rtype: str
        """
        assignees: str = " OR ".join([f"{assignee}: {self.login}" for assignee in self._assignees_project])
        return "".join(("(", assignees, ")"))

    @cached_property
    def _params_query(self) -> str:
        """
        Gets the "query" params value.

        :return: the params attribute value.
        :rtype: str
        """
        return " AND ".join((self._params_state, self._params_assignees))

    @cached_property
    def params(self) -> tuple[tuple[str, str], ...]:
        return (
            ("fields", self._params_field),
            ("query", self._params_query),
            ("customFields", self._params_custom_field_state),
            ("customFields", self._params_custom_field_deadline),
            ("customFields", self._params_custom_field_priority),
        )


class FullUserProcessor:
    _sender_email: str = "tarasov-a@protei.ru"
    test_message_attributes: MessageAttributes = MessageAttributes(
        f"Оповещение о задачах <{_sender_email}>",
        "Тестирование Задачи YouTrack, Исполнители_Doc/Doc_ST/Archive/Archive_ST",
        "text/plain; charset=UTF-8; format=flowed",
        "8bit",
        "ru",
        "1.0",
        f"<{_sender_email}>"
    )
    message_attributes: MessageAttributes = MessageAttributes(
        f"Оповещение о задачах <{_sender_email}>",
        "Задачи YouTrack, Исполнители_Doc/Doc_ST/Archive/Archive_ST",
        "text/plain; charset=UTF-8; format=flowed",
        "8bit",
        "ru",
        "1.0",
        f"<{_sender_email}>"
    )
    _user_names: dict[str, str] = {
        "bochkova": "Софья Бочкова",
        "brovko": "Максим Бровко",
        "chursin": "Алексей Чурсин",
        "demyanenko": "Светлана Демьяненко",
        "fesenko": "Виктор Фесенко",
        "kuksina": "Ольга Куксина",
        "lyalina": "Кристина Лялина",
        "matyushina": "Вероника Мозглякова",
        "mazyarova": "Дарья Мазярова",
        "nigrej": "Дмитрий Нигрей",
        "nikitina": "Наталья Никитина",
        "sobolev-p": "Соболев Павел",
        "vykhodtsev": "Алексей Выходцев",
        "tarasov-a": "Андрей Тарасов"
    }

    def __init__(
            self,
            file_name: str = "issues.json", *,
            is_test: bool = False):
        self.json_file: JSONFile = JSONFile(file_name)
        self.json_file.read()
        self.is_test: bool = is_test
        self.session: Session = Session()
        self.logins: list[str] = [*self.json_file.content.keys()]
        self.smtp_server: MailServer = MailServer()
        self._users: list[User] = []

    @property
    def users(self):
        return self._users

    @users.setter
    def users(self, value):
        self._users = value

    def __repr__(self):
        return f"<{self.__class__.__name__}(logins={self.logins}, is_test={self.is_test})>"

    def __str__(self):
        return f"{self.__class__.__name__}: logins = {self.logins}, test: {self.is_test}"

    def to_json_file(self):
        content: dict[str, list[str]] = dict()
        for user in self.users:
            content[user.login] = []
            issues: list[str] = list(map(lambda x: x.issue, user.responses))
            content[user.login].extend(issues)
        return content

    def send_mails(self, messages: Sequence[MessageYoutrack] = None):
        self.smtp_server.send_emails(messages)


class User(FullUserProcessor):
    def __init__(self, file_name: str, login: str, *, is_test: bool = False, is_init: bool = False):
        super().__init__(file_name, is_test=is_test)
        self.login: str = login
        self.is_init: bool = is_init

        if self.is_test:
            self._recipient_email: str = f"{self._sender_email}"
        else:
            self._recipient_email: str = f"{self.login}@protei.ru"

    def __repr__(self):
        return f"<{self.__class__.__name__}(login={self.login}, is_test={self.is_test})>"

    def __str__(self):
        return f"Пользователь {self._user_names[self.login]}, {self._recipient_email}"

    def name(self):
        return self._user_names[self.login]

    @property
    def _request_params(self) -> tuple[tuple[str, str], ...]:
        return _RequestParams(self.login).params

    @property
    def _youtrack_request(self) -> RequestYoutrack:
        return RequestYoutrack(self.session, self._request_params)

    @property
    def responses(self) -> list[ResponseYoutrack]:
        return self._youtrack_request.parse_response()

    @property
    def message_processor(self) -> MessageProcessor:
        return MessageProcessor(self.json_file, self.login, self.responses)

    def message_text(self) -> Optional[str]:
        _message_text: Optional[str] = self.message_processor.message_text(self.is_init)
        logger.info(f"Текст сообщения:\n{_message_text}")
        return _message_text

    def message_youtrack(self) -> MessageYoutrack:
        if self.is_test:
            msg_attrs: MessageAttributes = self.test_message_attributes
        else:
            msg_attrs: MessageAttributes = self.message_attributes
        return MessageYoutrack(self._sender_email, self._recipient_email, self.message_text(), msg_attrs)

    def send_mails(self, messages: Sequence[MessageYoutrack] = None):
        return NotImplemented

    def add_to_full_user_processor(self):
        super().users.append(self)
