__title__ = "sending_emails"
__description__ = "Get issues from the Youtrack and send them in the emails."
__url__ = "https://gitlab.com/andrewtarsudo"
__version__ = "1.0.1"
__build__ = 0x010001
__author__ = "Andrew Tar"
__license__ = "MIT"
