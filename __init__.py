import importlib
import json
import logging
import subprocess
import warnings
from logging import NullHandler
from pathlib import Path
from sys import executable

import __version__ as v

__title__ = v.__title__
__description__ = v.__description__
__url__ = v.__url__
__version__ = v.__version__
__build__ = v.__build__
__author__ = v.__author__
__license__ = v.__license__


_parent_path: Path = Path(__file__).resolve().parent
__path__.append(_parent_path)
_module_path: Path = _parent_path.joinpath("sending_emails").resolve()
__path__.append(_module_path)

warnings.simplefilter("ignore")


def install_package(package: str):
    try:
        subprocess.run([executable, "-m", "pip", "install", package]).check_returncode()
    except subprocess.CalledProcessError as error:
        print(f"{error.__class__.__name__}\nКод ответа {error.returncode}\nОшибка {error.output}")
        print(f"Не удалось импортировать пакет `{package}`.")
        raise error
    except OSError as error:
        print(f"{error.__class__.__name__}\nФайл {error.filename}\nОшибка {error.strerror}")
        print(f"Не удалось импортировать пакет `{package}`")
        raise error
    else:
        print(f"Установлен пакет {package}")

_packages: tuple[str, ...] = ("requests", "loguru")

for _package in _packages:
    try:
        importlib.import_module(_package)
    except ModuleNotFoundError:
        install_package(_package)
    except ImportError:
        install_package(_package)

_issues_json_file: Path = _module_path.with_name("issues.json").resolve()

if not _issues_json_file.exists():
    _issues_json_file.touch(exist_ok=True)
    text: dict[str, list[str]] = {
        "bochkova": [],
        "brovko": [],
        "chursin": [],
        "demyanenko": [],
        "fesenko": [],
        "kuksina": [],
        "lyalina": [],
        "matyushina": [],
        "mazyarova": [],
        "nigrej": [],
        "nikitina": [],
        "sobolev-p": [],
        "vykhodtsev": [],
        "tarasov-a": []
    }
    with open(_issues_json_file, "w") as file:
        file.write(json.dumps(text, indent=2))

_log_folder: Path = _module_path.joinpath("logs").resolve()

if not _log_folder.exists():
    _log_folder.mkdir(parents=True)

logging.getLogger(__name__).addHandler(NullHandler())
